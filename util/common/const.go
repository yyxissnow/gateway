package common

const (
	RedisFlowDayKey  = "flow_day_count"
	RedisFlowHourKey = "flow_hour_count"

	FlowTotal         = "flow_total"
	FlowServicePrefix = "flow_service_"
	FLowAppPrefix     = "flow_app_"

	JwtSignKey = "gateway_sign"
	JwtExpires = 60 * 60 * 3

	HttpProxyType = 0
	TcpProxyType  = 1
	GrpcProxyType = 2
)

var ProxyTypeMap = map[int8]string{
	HttpProxyType: "HTTP",
	TcpProxyType:  "TCP",
	GrpcProxyType: "GRPC",
}
