package common

import (
	"gateway/dao/model"
	"gateway/setup/db"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func InStringSlice(slice []string, str string) bool {
	for _, item := range slice {
		if str == item {
			return true
		}
	}
	return false
}

func InitHttpServer(handler *gin.Engine, port string) *http.Server {
	return &http.Server{
		Addr:         port,
		Handler:      handler,
		ReadTimeout:  time.Duration(3) * time.Second,
		WriteTimeout: time.Duration(3) * time.Second,
	}
}

func InitDBTable() {
	db.DB.AutoMigrate(
		&model.App{},
		&model.User{},
		&model.Service{},
		&model.HttpRule{},
		&model.ServiceControl{},
		&model.LoadBalance{},
	)
}
