package common

import (
	"fmt"
	"gateway/setup/redis"
	"log"
	"sync/atomic"
	"time"
)

type RedisFlowCountService struct {
	AppID       string
	Interval    time.Duration
	QPS         int64
	Unix        int64
	TickerCount int64
	TotalCount  int64
}

func NewRedisFlowCountService(appID string, interval time.Duration) *RedisFlowCountService {
	counter := &RedisFlowCountService{
		AppID:    appID,
		Interval: interval,
		QPS:      0,
		Unix:     0,
	}
	go func() {
		defer func() {
			if err := recover(); err != nil {
				log.Printf("%s redis flow counter err:%v", appID, err)
			}
		}()
		for range time.Tick(interval) {
			tickerCount := atomic.LoadInt64(&counter.TickerCount)
			atomic.StoreInt64(&counter.TickerCount, 0)
			now := time.Now()
			day := counter.GetDayKey(now)
			hour := counter.GetHourKey(now)

			pipeline := redis.GetRedisClient().Pipeline()
			pipeline.IncrBy(day, tickerCount)
			pipeline.Expire(day, time.Duration(48)*time.Hour)
			pipeline.IncrBy(hour, tickerCount)
			pipeline.Expire(hour, time.Duration(48)*time.Hour)
			if _, err := pipeline.Exec(); err != nil {
				log.Printf("%s redis pipeline err:%v", counter.AppID, err)
				continue
			}

			totalCount, err := counter.GetDayData(now)
			if err != nil {
				log.Printf("%s redis get day data err:%v", counter.AppID, err)
				continue
			}
			nowUnix := time.Now().Unix()
			if counter.Unix == 0 {
				counter.Unix = nowUnix
				continue
			}
			tickerCount = totalCount - counter.TotalCount
			if nowUnix > counter.Unix {
				counter.TotalCount = totalCount
				counter.QPS = tickerCount / (nowUnix - counter.Unix)
				counter.Unix = time.Now().Unix()
			}
		}
	}()
	return counter
}

func (r *RedisFlowCountService) GetDayKey(t time.Time) string {
	day := t.In(time.Local).Format("20060102")
	return fmt.Sprintf("%s_%s_%s", RedisFlowDayKey, day, r.AppID)
}

func (r *RedisFlowCountService) GetHourKey(t time.Time) string {
	hour := t.In(time.Local).Format("2006010215")
	return fmt.Sprintf("%s_%s_%s", RedisFlowHourKey, hour, r.AppID)
}

func (r *RedisFlowCountService) GetDayData(t time.Time) (int64, error) {
	return redis.GetRedisClient().Get(r.GetDayKey(t)).Int64()
}

func (r *RedisFlowCountService) GetHourData(t time.Time) (int64, error) {
	return redis.GetRedisClient().Get(r.GetHourKey(t)).Int64()
}

func (r *RedisFlowCountService) Increase() {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				log.Printf("increase err:%v", err)
			}
		}()
		atomic.AddInt64(&r.TickerCount, 1)
	}()
}
