package common

import (
	"sync"
	"time"
)

var FlowCounterHandler *FlowCounter

type FlowCounter struct {
	RedisFlowCountMap   map[string]*RedisFlowCountService
	RedisFlowCountSlice []*RedisFlowCountService
	Locker              sync.RWMutex
}

func NewFlowCounter() *FlowCounter {
	return &FlowCounter{
		RedisFlowCountMap:   map[string]*RedisFlowCountService{},
		RedisFlowCountSlice: []*RedisFlowCountService{},
		Locker:              sync.RWMutex{},
	}
}

func init() {
	FlowCounterHandler = NewFlowCounter()
}

func (c *FlowCounter) GetCounter(serviceName string) *RedisFlowCountService {
	for _, item := range c.RedisFlowCountSlice {
		if item.AppID == serviceName {
			return item
		}
	}
	newCounter := NewRedisFlowCountService(serviceName, 1*time.Second)
	c.RedisFlowCountSlice = append(c.RedisFlowCountSlice, newCounter)
	c.Locker.Lock()
	defer c.Locker.Unlock()
	c.RedisFlowCountMap[serviceName] = newCounter
	return newCounter
}
