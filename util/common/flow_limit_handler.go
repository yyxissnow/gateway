package common

import (
	"golang.org/x/time/rate"
	"sync"
)

var FlowLimiterHandler *FlowLimiter

type FlowLimiter struct {
	FlowLimiterMap   map[string]*FlowLimiterItem
	FlowLimiterSlice []*FlowLimiterItem
	Locker           sync.RWMutex
}

type FlowLimiterItem struct {
	ServiceName string        `json:"service_name"`
	Limiter     *rate.Limiter `json:"limiter"`
}

func NewFlowLimiter() *FlowLimiter {
	return &FlowLimiter{
		FlowLimiterMap:   map[string]*FlowLimiterItem{},
		FlowLimiterSlice: []*FlowLimiterItem{},
		Locker:           sync.RWMutex{},
	}
}

func init() {
	FlowLimiterHandler = NewFlowLimiter()
}

func (f *FlowLimiter) GetCounter(serviceName string, qps float64) *rate.Limiter {
	for _, item := range f.FlowLimiterSlice {
		if item.ServiceName == serviceName {
			return item.Limiter
		}
	}
	newLimiter := rate.NewLimiter(rate.Limit(qps), int(qps*3)) // 分别传入每秒生成的token数和最大桶的数量
	item := FlowLimiterItem{
		ServiceName: serviceName,
		Limiter:     newLimiter,
	}
	f.FlowLimiterSlice = append(f.FlowLimiterSlice, &item)
	f.Locker.Lock()
	defer f.Locker.Unlock()
	f.FlowLimiterMap[serviceName] = &item
	return item.Limiter
}
