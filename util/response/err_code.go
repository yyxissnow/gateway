package response

type ErrorCode int

const (
	ErrBindJson     ErrorCode = 1000
	ErrRequestParam           = 1001
	ErrDecodeString           = 1002
	ErrDeCodeToken            = 1003
	ErrEnCodeToken            = 1004

	ErrDashboard           = 2000
	ErrDBAppIDExist        = 2001
	ErrDBCreateApp         = 2002
	ErrDBNoFindApp         = 2003
	ErrDBUpdateApp         = 2004
	ErrDBDeleteApp         = 2005
	ErrDBGetApp            = 2006
	ErrDBFindApps          = 2007
	ErrDBGetAppNum         = 2008
	ErrDBGetAppDetail      = 2009
	ErrNoFindApp           = 2050
	ErrDBCreateAdmin       = 2060
	ErrDBLoginAdmin        = 2061
	ErrDBGetAdmin          = 2062
	ErrDBFindServices      = 2100
	ErrDBGetServiceDetail  = 2101
	ErrDBNoFindService     = 2103
	ErrDBDeleteService     = 2104
	ErrDBCreateHttpService = 2105
	ErrDBUpdateHttpService = 2106
	ErrDBGetServiceNum     = 2107
	ErrDBGroupByProxyType  = 2108
	ErrProxyTypeMap        = 2109

	ErrProxy                     = 5000
	ErrMiddlewarePanic           = 5001
	ErrNoFindService             = 5002
	ErrMaxAppQPDFlow             = 5003
	ErrNoGetCtxService           = 5004
	ErrNoMatchApp                = 5005
	ErrNoGetCtxApp               = 5006
	ErrMaxAppLimitFlow           = 5007
	ErrMaxServiceLimitFlow       = 5008
	ErrMaxClientLimitFlow        = 5009
	ErrNoInServiceWhiteList      = 5010
	ErrInServiceBlackList        = 5011
	ErrNoFindServiceLoadBalancer = 5012
)
