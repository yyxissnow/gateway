package router

import (
	"gateway/control"
	"gateway/middleware"
	"gateway/middleware/http_proxy_middleware"
	"github.com/gin-gonic/gin"
)

func InitProxyRouter() *gin.Engine {
	router := gin.New()
	oauth := router.Group("/gateway/oauth")
	{
		oauth.POST("/token", control.GetToken)
	}
	router.Use(
		middleware.RecoveryMiddleware(),
		http_proxy_middleware.HttpAccessNodeMiddleware(),
		http_proxy_middleware.HttpJwtAuthTokenMiddleware(),
		http_proxy_middleware.HttpAppFlowCountMiddleware(),
		http_proxy_middleware.HttpAppFlowLimitMiddleware(),
		http_proxy_middleware.HttpServiceFlowCountMiddleware(),
		http_proxy_middleware.HttpServiceFlowLimitMiddleware(),
		http_proxy_middleware.HttpWhiteListMiddleware(),
		http_proxy_middleware.HttpBlackListMiddleware(),
		http_proxy_middleware.HttpHeaderTransferMiddleware(),
		http_proxy_middleware.HttpUrlRewriteMiddleware(),
		http_proxy_middleware.HttpReverseProxyMiddleware(),
	)
	return router
}
