package router

import (
	"gateway/control"
	"github.com/gin-gonic/gin"
)

func InitDashboardRouter() *gin.Engine {
	r := gin.New()
	gateway := r.Group("/gateway")
	admin := gateway.Group("/admin")
	{
		admin.POST("/create", control.CreateUser)
		admin.POST("/login", control.LoginUser)
		admin.GET("/logout", control.LogoutUser)
		admin.GET("/info", control.GetInfo)
	}
	app := gateway.Group("/app")
	{
		app.POST("/create", control.CreateApp)
		app.PUT("/update", control.UpdateApp)
		app.DELETE("/delete", control.DeleteApp)
		app.GET("/get", control.GetApp)
		app.GET("/find", control.FindApps)
		app.GET("/detail", control.GetAppDetail)
		app.GET("/statistics", control.StatisticsApp)
	}
	service := gateway.Group("/service")
	{
		service.GET("/find", control.FindServices)
		service.GET("/detail", control.GetServiceDetail)
		service.GET("/statistics", control.ServiceStatistics)
		service.DELETE("/delete", control.DeleteService)
		service.POST("/create_http", control.CreateHttpService)
		service.PUT("/update_http", control.UpdateHttpService)
	}
	dashboard := gateway.Group("/dashboard")
	{
		dashboard.GET("/panel_group_data", control.GetPanelGroupData)
		dashboard.GET("/flow_statistics", control.StatisticsFlow)
		dashboard.GET("/service_statistics", control.StatisticsService)
	}
	return r
}
