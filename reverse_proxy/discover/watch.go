package discover

import (
	"context"
	"fmt"
	"gateway/dao/manager"
	"gateway/setup/etcd"
	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
	"log"
	"sync"
)

var ServerManager *ServerDiscover

type ServerDiscover struct {
	AliveAddr map[string]struct{} //相当于是集合
	AppToAddr map[string]string   //etcd的watch delete只能拿到key
	lock      sync.Mutex
	init      sync.Once
}

func init() {
	ServerManager = NewServerManger()
}

func NewServerManger() *ServerDiscover {
	return &ServerDiscover{
		AliveAddr: make(map[string]struct{}),
		AppToAddr: make(map[string]string),
		lock:      sync.Mutex{},
		init:      sync.Once{},
	}
}

func (s *ServerDiscover) LoadOnce(path string) {
	s.init.Do(func() {
		s.LoadServer(path)
		go s.WatchServer(path)
	})
}

func (s *ServerDiscover) LoadServer(path string) {
	res, err := etcd.GetEtcd().Get(context.TODO(), path, clientv3.WithPrefix())
	if err != nil {
		log.Fatalf("etcd load server err:%s", err.Error())
	}
	for _, item := range res.Kvs {
		fmt.Println(string(item.Key) + " register success " + string(item.Value))
		s.AddNode(string(item.Key), string(item.Value))
	}
	s.LoadOnce(path)
}

func (s *ServerDiscover) WatchServer(path string) {
	ch := etcd.GetEtcd().Watch(context.Background(), path, clientv3.WithPrefix())
	for watchResp := range ch {
		for _, event := range watchResp.Events {
			switch event.Type {
			case mvccpb.PUT:
				fmt.Println(string(event.Kv.Key) + " register success " + string(event.Kv.Value))
				s.AddNode(string(event.Kv.Key), string(event.Kv.Value))
			case mvccpb.DELETE:
				fmt.Println(s.AppToAddr[string(event.Kv.Key)] + " delete success")
				s.DelNode(string(event.Kv.Key))
			}
		}
	}
}

func (s *ServerDiscover) AddNode(app, node string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if _, ok := s.AliveAddr[node]; !ok {
		s.AliveAddr[node] = struct{}{}
		s.AppToAddr[app] = node
		manager.LoadBalancerHandler.Update("Add", node)
	}
}

func (s *ServerDiscover) DelNode(app string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if _, ok := s.AppToAddr[app]; ok {
		delete(s.AliveAddr, s.AppToAddr[app])
		manager.LoadBalancerHandler.Update("Del", s.AppToAddr[app])
		delete(s.AppToAddr, app)
	}
}
