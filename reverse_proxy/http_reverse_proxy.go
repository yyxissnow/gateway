package reverse_proxy

import (
	"gateway/reverse_proxy/load_balance"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

func NewLoadBalanceReverseProxy(c *gin.Context, lb load_balance.LoadBalance, trans *http.Transport) *httputil.ReverseProxy {
	//请求协调者
	director := func(req *http.Request) { // 代理请求 req为要转发的req，可以进行复写
		nextAddr, err := lb.Get()
		if err != nil || nextAddr == "" {
			// TODO:当没有取到地址时的应对方法和此时的错误处理机制
			log.Printf("get load balancer addr is null")
		}
		target, err := url.Parse(nextAddr)
		if err != nil {
			log.Printf("parse load balancer addr err:%v", err)
		}
		//http://127.0,0.1:8080/test/find?name=snow
		//RawQuery:name=snow
		//Scheme:http
		//Host:127.0.0.1:8080
		//target是拿到目标的实际地址，req是准备请求的方法，要进行相应赋值
		targetQuery := target.RawQuery
		req.URL.Scheme = target.Scheme //协议的重新定义
		req.URL.Host = target.Host
		//target_path:xxx/abc + req_path:/def = xxx/abc/def
		req.URL.Path = singleJoiningSlash(target.Path, req.URL.Path)
		req.Host = target.Host
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			req.Header.Set("User-Agent", "user-agent")
		}
	}

	//更改返回内容
	modifyFunc := func(resp *http.Response) error {
		//Connection标记发起方与第一代理的状态
		//Upgrade:协议升级(websocket、http2)
		if strings.Contains(resp.Header.Get("Connection"), "Upgrade") {
			return nil
		}
		return nil
	}

	//错误回调 ：关闭real_server时测试，错误回调
	//范围：transport.RoundTrip发生的错误、以及ModifyResponse发生的错误
	errFunc := func(w http.ResponseWriter, r *http.Request, err error) {
		response.ResError(c, response.ErrProxy, err.Error())
	}
	return &httputil.ReverseProxy{Director: director, ModifyResponse: modifyFunc, Transport: trans, ErrorHandler: errFunc}
}

func singleJoiningSlash(target, req string) string {
	isTarget := strings.HasSuffix(target, "/")
	isReq := strings.HasPrefix(req, "/")
	switch {
	case isTarget && isReq:
		return target + req[1:]
	case !isTarget && !isReq:
		return target + "/" + req
	}
	return target + req
}
