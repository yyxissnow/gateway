package load_balance

type LoadBalance interface {
	Add(string) error
	Del(string) error
	Get() (string, error)
	Update(string, string) error
}
