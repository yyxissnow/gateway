package load_balance

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type RandomBalance struct {
	aliveAddresses  []string
	targetAddresses []string
}

func InitRandom(ipList map[string]string) *RandomBalance {
	r := &RandomBalance{}
	for node, _ := range ipList {
		r.targetAddresses = append(r.targetAddresses, node)
	}
	return r
}

func (r *RandomBalance) Add(addr string) error {
	if len(addr) == 0 {
		return errors.New("addresses is null")
	}
	for _, target := range r.targetAddresses {
		if target == addr {
			r.aliveAddresses = append(r.aliveAddresses, addr)
		}
	}
	return nil
}

func (r *RandomBalance) Del(addr string) error {
	if len(addr) == 0 {
		return errors.New("address is null")
	}
	var newAddr []string
	for _, old := range r.aliveAddresses {
		if old != addr {
			newAddr = append(newAddr, old)
		}
	}
	r.aliveAddresses = newAddr
	return nil
}

func (r *RandomBalance) Get() (string, error) {
	if len(r.aliveAddresses) == 0 {
		return "", nil
	}
	rand.Seed(time.Now().UnixNano())
	index := rand.Intn(len(r.aliveAddresses))
	return r.aliveAddresses[index], nil
}

func (r *RandomBalance) Update(method, node string) error {
	if method == "Add" {
		return r.Add(node)
	} else if method == "Del" {
		return r.Del(node)
	}
	return errors.New("update method err")
}

func (r *RandomBalance) Print() {
	for _, load := range r.targetAddresses {
		fmt.Println(load)
	}
}
