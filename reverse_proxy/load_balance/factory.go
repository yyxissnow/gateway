package load_balance

import (
	"gateway/dao/model"
)

func LoadBalanceFactor(t model.LoadBalanceType, ipList map[string]string) LoadBalance {
	//TODO:添加其他的负载均衡方法
	//观察者模式
	switch t {
	case model.Random:
		return InitRandom(ipList)
	case model.RoundRobin:
		return InitRoundRobin(ipList)
	default:
		return InitRandom(ipList)
	}
}
