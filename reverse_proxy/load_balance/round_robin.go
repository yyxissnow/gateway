package load_balance

import (
	"errors"
	"fmt"
)

type RoundRobinBalance struct {
	curIndex        int
	aliveAddresses  []string
	targetAddresses []string
}

func InitRoundRobin(ipList map[string]string) *RoundRobinBalance {
	r := &RoundRobinBalance{}
	for node, _ := range ipList {
		r.targetAddresses = append(r.targetAddresses, node)
	}
	return r
}

func (r *RoundRobinBalance) Add(addr string) error {
	if len(addr) == 0 {
		return errors.New("address is null")
	}
	for _, target := range r.targetAddresses {
		if target == addr {
			r.aliveAddresses = append(r.aliveAddresses, addr)
		}
	}
	return nil
}

func (r *RoundRobinBalance) Del(addr string) error {
	if len(addr) == 0 {
		return errors.New("address is null")
	}
	var newAddr []string
	for _, old := range r.aliveAddresses {
		if old != addr {
			newAddr = append(newAddr, old)
		}
	}
	r.aliveAddresses = newAddr
	return nil
}

func (r *RoundRobinBalance) Get() (string, error) {
	if len(r.aliveAddresses) == 0 {
		return "", nil
	}
	if r.curIndex >= len(r.aliveAddresses) {
		r.curIndex = 0
	}
	addr := r.aliveAddresses[r.curIndex]
	r.curIndex++
	return addr, nil
}

func (r *RoundRobinBalance) Update(method, node string) error {
	if method == "Add" {
		return r.Add(node)
	} else if method == "Del" {
		return r.Del(node)
	}
	return errors.New("update method err")
}

func (r *RoundRobinBalance) Print() {
	for _, load := range r.targetAddresses {
		fmt.Println(load)
	}
}
