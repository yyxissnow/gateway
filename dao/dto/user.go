package dto

type CreateUserREQ struct {
	Name         string `json:"name"`
	Password     string `json:"password"`
	Avatar       string `json:"avatar"`
	Introduction string `json:"introduction"`
}

type LoginUserREQ struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type LoginUserACK struct {
	Token string `json:"token"`
}

type GetUserACK struct {
	ID           uint64   `json:"id"`
	Name         string   `json:"name"`
	Avatar       string   `json:"avatar"`
	Introduction string   `json:"introduction"`
	Roles        []string `json:"roles"`
}
