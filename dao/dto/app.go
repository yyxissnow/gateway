package dto

type AddAppREQ struct {
	AppCode     string `json:"app_code"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Secret      string `json:"secret"`
	WhiteIPS    string `json:"white_ips"`
	QPS         int64  `json:"qps"`
	QPD         int64  `json:"qpd"`
}

type UpdateAppREQ struct {
	ID          uint64 `json:"id"`
	AppCode     string `json:"app_code"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Secret      string `json:"secret"`
	WhiteIPS    string `json:"white_ips"`
	QPS         int64  `json:"qps"`
	QPD         int64  `json:"qpd"`
}

type GetAppREQ struct {
	AppCode string `json:"app_code"`
}

type FindAppsREQ struct {
	Keyword  string `json:"keyword"`
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
}

type FindAppsACK struct {
	ID          uint64 `json:"id"`
	AppCode     string `json:"app_code"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Secret      string `json:"secret"`
	WhiteIPS    string `json:"white_ips"`
	QPS         int64  `json:"qps"`
	QPD         int64  `json:"qpd"`
	RealQPD     int64  `json:"real_qpd"`
	RealQPS     int64  `json:"real_qps"`
}

type StatisticsAppACK struct {
	Today     []int64 `json:"today"`
	Yesterday []int64 `json:"yesterday"`
}
