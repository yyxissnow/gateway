package dto

type GetPanelGroupDataACK struct {
	ServiceNum      int64 `json:"service_num"`
	AppNum          int64 `json:"app_num"`
	CurrentQPS      int64 `json:"current_qps"`
	TodayRequestNum int64 `json:"today_request_num"`
}

type StatisticsFlowACK struct {
	Today     []int64 `json:"today"`
	Yesterday []int64 `json:"yesterday"`
}

type StatisticsServiceACK struct {
	Legend []string    `json:"legend"`
	Data   interface{} `json:"data"`
}
