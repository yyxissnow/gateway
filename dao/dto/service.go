package dto

import (
	"gateway/dao/model"
)

type ServiceStatisticsACK struct {
	Today     []int64 `json:"today"` //今日流量
	Yesterday []int64 `json:"yesterday"`
}

type FindServicesACK struct {
	ID          uint64          `json:"id"`
	Name        string          `json:"name"`
	Description string          `json:"description"`
	ProxyType   model.ProxyType `json:"proxy_type"`
	Address     string          `json:"address"`
	QPS         int64           `json:"qps"`
	QPD         int64           `json:"qpd"`
	Nodes       int             `json:"nodes"` //总节点数
}
