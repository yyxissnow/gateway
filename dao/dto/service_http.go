package dto

import (
	"gateway/dao/model"
)

type CreateHttpServiceREQ struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	AppCode     string `json:"app_code"`

	RuleType       model.RuleType `json:"rule_type"`
	Rule           string         `json:"rule"`
	NeedHttps      bool           `json:"need_https"`
	NeedWebSocket  bool           `json:"need_web_socket"`
	UrlRewrite     string         `json:"url_rewrite"`
	HeaderTransfer string         `json:"header_transfer"`

	OpenAuth          bool   `json:"open_auth"`
	BackList          string `json:"back_list"`
	WhiteList         string `json:"white_list"`
	ClientIPFlowLimit int    `json:"client_ip_flow_limit"`
	ServiceFlowLimit  int    `json:"service_flow_limit"`

	LoadBalanceType        model.LoadBalanceType `json:"load_balance_type"`
	IPList                 string                `json:"ip_list"`
	WeightList             string                `json:"weight_list"`
	UpstreamConnectTimeout int                   `json:"upstream_connect_timeout"`
	UpstreamHeaderTimeout  int                   `json:"upstream_header_timeout"`
	UpstreamIdleTimeout    int                   `json:"upstream_idle_timeout"`
	UpstreamMaxIdle        int                   `json:"upstream_max_idle"`
}

type UpdateServiceHttpREQ struct {
	ServiceID   uint64 `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	AppCode     string `json:"app_code"`

	RuleID         uint64         `json:"rule_id"`
	RuleType       model.RuleType `json:"rule_type"`
	Rule           string         `json:"rule"`
	NeedStripUri   bool           `json:"need_strip_uri"`
	NeedHttps      bool           `json:"need_https"`
	NeedWebSocket  bool           `json:"need_web_socket"`
	UrlRewrite     string         `json:"url_rewrite"`
	HeaderTransfer string         `json:"header_transfer"`

	OpenAuth          bool   `json:"open_auth"`
	BackList          string `json:"black_list"`
	WhiteList         string `json:"white_list"`
	ClientIPFlowLimit int    `json:"client_ip_flow_limit"`
	ServiceFlowLimit  int    `json:"service_flow_limit"`

	LoadBalanceType        model.LoadBalanceType `json:"load_balance_type"`
	IPList                 string                `json:"ip_list"`
	WeightList             string                `json:"weight_list"`
	UpstreamConnectTimeout int                   `json:"upstream_connect_timeout"`
	UpstreamHeaderTimeout  int                   `json:"upstream_header_timeout"`
	UpstreamIdleTimeout    int                   `json:"upstream_idle_timeout"`
	UpstreamMaxIdle        int                   `json:"upstream_max_idle"`
}
