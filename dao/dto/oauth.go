package dto

type GetTokenRSP struct {
	Token     string `json:"token"`
	Expires   int    `json:"expires"`
	TokenType string `json:"token_type"`
	Scope     string `json:"scope"`
}
