package manager

import (
	"errors"
	"gateway/dao/model"
	"gateway/reverse_proxy/load_balance"
	"log"
	"sync"
)

type LoadBalancer struct {
	LoadBalanceMap   map[string]*LoadBalancerItem
	LoadBalanceSlice []*LoadBalancerItem
	Locker           sync.RWMutex
	init             sync.Once
}

type LoadBalancerItem struct {
	LoadBalance load_balance.LoadBalance
	ServiceName string
}

var LoadBalancerHandler *LoadBalancer

func init() {
	LoadBalancerHandler = NewLoadBalancer()
}

func NewLoadBalancer() *LoadBalancer {
	return &LoadBalancer{
		LoadBalanceMap:   map[string]*LoadBalancerItem{},
		LoadBalanceSlice: []*LoadBalancerItem{},
		Locker:           sync.RWMutex{},
	}
}

func (l *LoadBalancer) LoadOnce() {
	//TODO：实时和定时更新负载均衡管理器
	//TODO：当proxy重启时需要将etcd的地址添加到load balancer（因为上游服务器是没有重启的，watch已经收不到add）
	schema := "http://"
	l.init.Do(func() {
		for _, service := range ServiceManagerHandler.ServiceSlice {
			if service.HttpRule.NeedHttps == true {
				schema = "https://"
			}
			if service.ServiceInfo.ProxyType == model.Tcp || service.ServiceInfo.ProxyType == model.Grpc {
				schema = ""
			}
			ipList := service.LoadBalance.GetIPNodes()
			weight := service.LoadBalance.GetIPWeightList()
			ipConf := map[string]string{}
			for index, ip := range ipList {
				ipConf[schema+ip] = weight[index]
			}

			lb := load_balance.LoadBalanceFactor(service.LoadBalance.LoadBalanceType, ipConf)
			lbItem := &LoadBalancerItem{
				LoadBalance: lb,
				ServiceName: service.ServiceInfo.Name,
			}
			l.LoadBalanceSlice = append(l.LoadBalanceSlice, lbItem)
			l.Locker.Lock()
			l.LoadBalanceMap[service.ServiceInfo.Name] = lbItem
			l.Locker.Unlock()
		}
	})
}

func (l *LoadBalancer) GetLoadBalancer(service *model.ServiceDetail) (load_balance.LoadBalance, error) {
	for _, item := range l.LoadBalanceSlice {
		if item.ServiceName == service.ServiceInfo.Name {
			return item.LoadBalance, nil
		}
	}
	return nil, errors.New("not find load balancer")
}

func (l *LoadBalancer) Update(method, node string) {
	for _, item := range l.LoadBalanceSlice {
		if err := item.LoadBalance.Update(method, node); err != nil {
			log.Printf("%s load balancer %s %s err:%v", item.ServiceName, method, node, err)
			continue
		}
	}
}
