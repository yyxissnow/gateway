package manager

import (
	"gateway/dao/model"
	"net"
	"net/http"
	"sync"
	"time"
)

//TODO：每个service都创一个连接资源太浪费了

// Transport 连接池
type Transport struct {
	TransportMap   map[string]*TransportItem
	TransportSlice []*TransportItem
	Locker         sync.RWMutex
}

// TransportItem 每一个service都有自己独立的连接池
type TransportItem struct {
	Trans       *http.Transport
	ServiceName string
}

var TransportHandler *Transport

func init() {
	TransportHandler = NewTransport()
}

func NewTransport() *Transport {
	return &Transport{
		TransportMap:   map[string]*TransportItem{},
		TransportSlice: []*TransportItem{},
		Locker:         sync.RWMutex{},
	}
}

func (t *Transport) GetTransport(service *model.ServiceDetail) *http.Transport {
	for _, item := range t.TransportSlice {
		if item.ServiceName == service.ServiceInfo.Name {
			return item.Trans
		}
	}
	if service.LoadBalance.UpstreamConnectTimeout == 0 {
		service.LoadBalance.UpstreamConnectTimeout = 30
	}
	if service.LoadBalance.UpstreamMaxIdle == 0 {
		service.LoadBalance.UpstreamMaxIdle = 100
	}
	if service.LoadBalance.UpstreamIdleTimeout == 0 {
		service.LoadBalance.UpstreamIdleTimeout = 90
	}
	if service.LoadBalance.UpstreamHeaderTimeout == 0 {
		service.LoadBalance.UpstreamHeaderTimeout = 30
	}
	trans := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   time.Duration(service.LoadBalance.UpstreamConnectTimeout) * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          service.LoadBalance.UpstreamMaxIdle,
		IdleConnTimeout:       time.Duration(service.LoadBalance.UpstreamIdleTimeout) * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ResponseHeaderTimeout: time.Duration(service.LoadBalance.UpstreamHeaderTimeout) * time.Second,
	}

	transItem := &TransportItem{
		Trans:       trans,
		ServiceName: service.ServiceInfo.Name,
	}
	t.TransportSlice = append(t.TransportSlice, transItem)
	t.Locker.Lock()
	defer t.Locker.Unlock()
	t.TransportMap[service.ServiceInfo.Name] = transItem
	return trans
}
