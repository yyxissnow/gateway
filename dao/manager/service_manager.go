package manager

import (
	"errors"
	"gateway/dao/model"
	"github.com/gin-gonic/gin"
	"strings"
	"sync"
	"time"
)

var ServiceManagerHandler *ServiceManager

type ServiceManager struct {
	ServiceMap   map[string]*model.ServiceDetail //根据名字查service
	ServiceSlice []*model.ServiceDetail          //匹配遍历切片
	Locker       sync.RWMutex
	init         sync.Once
	err          error
}

func init() {
	ServiceManagerHandler = NewServiceManager()
}

func NewServiceManager() *ServiceManager {
	return &ServiceManager{
		ServiceMap:   map[string]*model.ServiceDetail{},
		ServiceSlice: []*model.ServiceDetail{},
		Locker:       sync.RWMutex{},
		init:         sync.Once{},
	}
}

func (s *ServiceManager) LoadOnce() {
	s.init.Do(func() {
		manager := dispatchConfiguration()
		s.Locker.Lock()
		defer s.Locker.Unlock()
		s.ServiceMap = manager.ServiceMap
		s.ServiceSlice = manager.ServiceSlice
		go s.tickerDispatch()
	})
}

// TickerDispatch 实时配置下发
func (s *ServiceManager) tickerDispatch() {
	for range time.Tick(time.Second * 5) {
		manager := dispatchConfiguration()
		s.Locker.Lock()
		s.ServiceMap = manager.ServiceMap
		s.ServiceSlice = manager.ServiceSlice
		s.Locker.Unlock()
	}
}

func (s *ServiceManager) AutoDispatch() {
	manager := dispatchConfiguration()
	s.Locker.Lock()
	defer s.Locker.Unlock()
	s.ServiceMap = manager.ServiceMap
	s.ServiceSlice = manager.ServiceSlice
}

func dispatchConfiguration() *ServiceManager {
	manager := &ServiceManager{
		ServiceMap:   map[string]*model.ServiceDetail{},
		ServiceSlice: []*model.ServiceDetail{},
		Locker:       sync.RWMutex{},
	}
	var services []model.Service
	if err := model.FindAllService(&services); err != nil {
		manager.err = err
		return manager
	}
	for _, service := range services {
		detail := model.ServiceDetail{}
		if err := detail.GetServiceDetail(service.ID); err != nil {
			manager.err = err
			return manager
		}
		manager.ServiceMap[service.Name] = &detail
		manager.ServiceSlice = append(manager.ServiceSlice, &detail)
	}
	return manager
}

func (s *ServiceManager) HttpAccessNode(c *gin.Context) (*model.ServiceDetail, error) {
	host := c.Request.Host
	host = host[:strings.Index(host, ":")]
	path := c.Request.URL.Path
	for _, detail := range s.ServiceSlice {
		if detail.ServiceInfo.ProxyType != model.Http {
			continue
		}
		if detail.HttpRule.RuleType == model.Domain && detail.HttpRule.Rule == host {
			return detail, nil
		}
		if detail.HttpRule.RuleType == model.UrlPrefix && strings.HasPrefix(path, detail.HttpRule.Rule) {
			return detail, nil
		}
	}
	return nil, errors.New("not matched service")
}
