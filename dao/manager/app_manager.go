package manager

import (
	"gateway/dao/model"
	"sync"
)

// TODO：租户可以不用在内存添加管理器，可以在配置下发时去数据库里拉

var AppManagerHandler *AppManager

type AppManager struct {
	AppMap   map[string]*model.App
	AppSlice []*model.App
	Locker   sync.RWMutex
	init     sync.Once
	err      error
}

func init() {
	AppManagerHandler = NewAppManager()
}

func NewAppManager() *AppManager {
	return &AppManager{
		AppMap:   map[string]*model.App{},
		AppSlice: []*model.App{},
		Locker:   sync.RWMutex{},
		init:     sync.Once{},
	}
}

func (a *AppManager) ExistApp(code, secret string) bool {
	for _, app := range a.AppSlice {
		if code == app.AppCode && secret == app.Secret {
			return true
		}
	}
	return false
}

func (a *AppManager) GetAppList() []*model.App {
	return AppManagerHandler.AppSlice
}

func (a *AppManager) LoadOnce() {
	a.init.Do(func() {
		var apps []model.App
		if err := model.FindAllApps(&apps); err != nil {
			panic(err)
		}
		a.Locker.Lock()
		defer a.Locker.Unlock()
		for _, app := range apps {
			a.AppMap[app.AppCode] = &app
			a.AppSlice = append(a.AppSlice, &app)
		}
	})
}
