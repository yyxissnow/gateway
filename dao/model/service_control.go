package model

import (
	"gateway/setup/db"
	"time"
)

//TODO：应该是安全配置

type ServiceControl struct {
	ID                uint64     `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt         time.Time  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt         time.Time  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt         *time.Time `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	ServiceID         uint64     `json:"service_id"`
	OpenAuth          bool       `json:"open_auth"`
	WhiteList         string     `json:"white_list"`
	BlackList         string     `json:"black_list"`
	ClientIPFlowLimit int        `json:"client_ip_flow_limit"` // 0就代表无限制
	ServiceFlowLimit  int        `json:"service_flow_limit"`
}

func (s *ServiceControl) FindServiceControlByServiceID() error {
	return db.DB.Model(ServiceControl{}).Where("service_id = ?", s.ServiceID).First(&s).Error
}
