package model

import (
	"gateway/setup/db"
	"strings"
	"time"
)

type LoadBalanceType uint8

const (
	Random LoadBalanceType = iota
	RoundRobin
	WeightRoundRobin
	ConsistentHash
)

type LoadBalance struct {
	ID              uint64          `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt       time.Time       `json:"created_at" gorm:"column:created_at"`
	UpdatedAt       time.Time       `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt       *time.Time      `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	ServiceID       uint64          `json:"service_id"`
	CheckMethod     int             `json:"check_method"`   //检测端口是否握手成功
	CheckTimeout    int             `json:"check_timeout"`  //检测超时时间
	CheckInterval   int             `json:"check_interval"` //检测间隔
	LoadBalanceType LoadBalanceType `json:"load_balance_type"`
	IsCheck         bool            `json:"is_check"` //是否开启非注册式健康检测
	IPList          string          `json:"ip_list"`
	WeightList      string          `json:"weight_list"` //IP权重值
	ForbidList      string          `json:"forbid_list"` //禁用IP列表

	UpstreamConnectTimeout int `json:"upstream_connect_timeout"` //上游建立连接超时
	UpstreamHeaderTimeout  int `json:"upstream_header_timeout"`  //上游获取header超时
	UpstreamIdleTimeout    int `json:"upstream_idle_timeout"`    //上游链接最大空闲时间
	UpstreamMaxIdle        int `json:"upstream_max_idle"`        //上游最大空闲链接数
}

func (l *LoadBalance) FindLoadBalanceByServiceID() error {
	return db.DB.Model(LoadBalance{}).Where("service_id = ?", l.ServiceID).First(&l).Error
}

func (l *LoadBalance) GetIPNodes() []string {
	return strings.Split(l.IPList, ",")
}

func (l *LoadBalance) GetIPWeightList() []string {
	return strings.Split(l.WeightList, ",")
}
