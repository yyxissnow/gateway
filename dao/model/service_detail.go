package model

import (
	"fmt"
	"gateway/setup/config"
	"github.com/pkg/errors"
)

type ServiceDetail struct {
	ServiceInfo    *Service        `json:"service_info"`
	HttpRule       *HttpRule       `json:"http_rule"`
	ServiceControl *ServiceControl `json:"service_control"`
	LoadBalance    *LoadBalance    `json:"load_balance"`
}

func (s *ServiceDetail) GetServiceDetail(id uint64) error {
	var service Service
	if err := service.GetServiceByID(id); err != nil {
		return errors.Wrap(err, "find service err")
	}
	s.ServiceInfo = &service
	control := ServiceControl{ServiceID: id}
	if err := control.FindServiceControlByServiceID(); err != nil {
		return errors.Wrap(err, "find service control err")
	}
	s.ServiceControl = &control
	switch service.ProxyType {
	case Http:
		rule := HttpRule{ServiceID: id}
		if err := rule.FirstHttpRuleByServiceID(); err != nil {
			return errors.Wrap(err, "find http rule err")
		}
		load := LoadBalance{ServiceID: id}
		if err := load.FindLoadBalanceByServiceID(); err != nil {
			return errors.Wrap(err, "find load balance err")
		}
		s.HttpRule = &rule
		s.LoadBalance = &load
	default:
		return errors.New("no service proxy type")
	}
	return nil
}

func (s *ServiceDetail) GetServiceAddress() string {
	//TODO:将详细信息配置到配置文件中
	clusterIP := "127.0.0.1"
	clusterPort := config.GetCfg().GatewayConf.Proxy
	clusterSSLPort := config.GetCfg().GatewayConf.Proxy
	if s.ServiceInfo.ProxyType == Http && s.HttpRule.RuleType == UrlPrefix && s.HttpRule.NeedHttps == true {
		return fmt.Sprintf("%s%s%s", clusterIP, clusterSSLPort, s.HttpRule.Rule)
	} else if s.ServiceInfo.ProxyType == Http && s.HttpRule.RuleType == UrlPrefix && s.HttpRule.NeedHttps == false {
		return fmt.Sprintf("%s%s%s", clusterIP, clusterPort, s.HttpRule.Rule)
	} else if s.ServiceInfo.ProxyType == Http && s.HttpRule.RuleType == Domain {
		return s.HttpRule.Rule
	}
	return "null"
}
