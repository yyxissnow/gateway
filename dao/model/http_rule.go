package model

import (
	"gateway/setup/db"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"time"
)

type RuleType uint8

const (
	Domain RuleType = iota
	UrlPrefix
)

type HttpRule struct {
	ID             uint64     `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt      time.Time  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt      time.Time  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt      *time.Time `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	ServiceID      uint64     `json:"service_id"`
	RuleType       RuleType   `json:"rule_type"`
	Rule           string     `json:"rule"` //当type为domain时显示域名，为url时显示前缀
	NeedHttps      bool       `json:"need_https"`
	NeedWebsocket  bool       `json:"need_websocket"`
	UrlRewrite     string     `json:"url_rewrite"`
	HeaderTransfer string     `json:"header_transfer"` //header转换支持add、del、edit,格式 add head_name head_value,del xxx xxx
}

func (h *HttpRule) JudgeHttpRuleExist() error {
	return db.DB.Model(HttpRule{}).Where("rule_type = ? and rule = ?", h.RuleType, h.Rule).First(&h).Error
}

func (h *HttpRule) JudgeHttpRuleExistNotSelf() error {
	return db.DB.Model(HttpRule{}).Where("rule_type = ? and rule = ?", h.RuleType, h.Rule).Not("service_id = ?", h.ServiceID).First(&h).Error
}

func (h *HttpRule) FirstHttpRuleByServiceID() error {
	return db.DB.Model(HttpRule{}).Where("service_id = ?", h.ServiceID).First(&h).Error
}

func (h *HttpRule) CreateHttpService(service *Service, control *ServiceControl, load *LoadBalance) error {
	return db.DB.Transaction(func(tx *gorm.DB) error {
		if err := service.JudgeServiceExistByName(); err != gorm.ErrRecordNotFound {
			return errors.New("service already exist!")
		}
		if err := h.JudgeHttpRuleExist(); err != gorm.ErrRecordNotFound {
			return errors.New("http rule already exist!")
		}
		if err := db.DB.Model(Service{}).Create(&service).Error; err != nil {
			return errors.Wrap(err, "service create err")
		}
		h.ServiceID = service.ID
		if err := db.DB.Model(HttpRule{}).Create(&h).Error; err != nil {
			return errors.Wrap(err, "http rule create err")
		}
		control.ServiceID = service.ID
		if err := db.DB.Where(ServiceControl{}).Create(&control).Error; err != nil {
			return errors.Wrap(err, "create service control err")
		}
		load.ServiceID = service.ID
		if err := db.DB.Model(LoadBalance{}).Create(&load).Error; err != nil {
			return errors.Wrap(err, "create load balance err")
		}
		return nil
	})
}

func UpdateHttpService(service *Service, rule *HttpRule, control *ServiceControl, load *LoadBalance) error {
	return db.DB.Transaction(func(tx *gorm.DB) error {
		// TODO:使用map[string]interface更新 无法更新0值 Save可以保存零值，但是会将时间更新成零值
		if err := db.DB.Model(Service{}).Where("id = ?", service.ID).Updates(service).Error; err != nil {
			return errors.Wrap(err, "update service err")
		}
		if err := db.DB.Model(HttpRule{}).Where("service_id = ?", rule.ServiceID).Updates(rule).Error; err != nil {
			return errors.Wrap(err, "update http rule err")
		}
		if err := db.DB.Where(ServiceControl{}).Where("service_id = ?", control.ServiceID).Updates(control).
			Update("white_list", control.WhiteList).Update("black_list", control.BlackList).Error; err != nil {
			return errors.Wrap(err, "update service control err")
		}
		if err := db.DB.Model(LoadBalance{}).Where("service_id = ?", load.ServiceID).Updates(load).Error; err != nil {
			return errors.Wrap(err, "update load balance err")
		}
		return nil
	})
}
