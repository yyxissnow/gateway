package model

import (
	"gateway/setup/db"
	"gateway/util/response"
	"time"
)

//TODO：租户密码不要明文保存

// App 租户
type App struct {
	ID          uint64     `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt   time.Time  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt   time.Time  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt   *time.Time `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	AppCode     string     `json:"app_code"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Secret      string     `json:"secret"`
	WhiteIPs    string     `json:"white_ips"`
	QPD         int64      `json:"qpd"` //日请求量限制
	QPS         int64      `json:"qps"` //每秒请求量限制
}

func (a *App) GetAppByID(id uint64) error {
	return db.DB.Model(App{}).Where("id = ?", id).First(a).Error
}

func (a *App) GetAppByName(name string) error {
	return db.DB.Model(App{}).Where("name = ?", name).First(a).Error
}

func (a *App) GetAppByAppID(code string) error {
	return db.DB.Model(App{}).Where("app_code = ?", code).First(a).Error
}

func (a *App) CreateApp() error {
	return db.DB.Model(App{}).Create(a).Error
}

func (a *App) UpdateApp() error {
	return db.DB.Model(App{}).Where("id = ?", a.ID).Updates(a).Error
}

func (a *App) DeleteApp() error {
	return db.DB.Model(App{}).Delete(a).Error
}

func FindAllApps(apps *[]App) error {
	return db.DB.Model(App{}).Find(apps).Error
}

func JudgeAppExist(code string) error {
	var app App
	return app.GetAppByAppID(code)
}

func FindApps(search response.PageSearch, apps *[]App) error {
	// TODO 是否分开判断
	// TODO add count
	result := db.DB.Model(App{}).Limit(search.Size).Offset((search.Page - 1) * search.Size).Order("id desc")
	if search.Keyword != "" {
		return result.Where("name like ? or app_id like ?", "%"+search.Keyword+"%", "%"+search.Keyword+"%").Find(&apps).Error
	}
	return result.Find(&apps).Error
}

func GetAppNum(num *int64) error {
	var apps []App
	return db.DB.Model(apps).Find(&apps).Count(num).Error
}
