package model

import (
	"gateway/setup/db"
	"gateway/util/response"
	"gorm.io/gorm"
	"time"
)

type ProxyType int8

const (
	Http ProxyType = iota
	Tcp
	Grpc
)

type Service struct {
	ID          uint64     `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt   time.Time  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt   time.Time  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt   *time.Time `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	Name        string     `json:"name"`
	AppCode     string     `json:"app_code"`
	Description string     `json:"description"`
	ProxyType   ProxyType  `json:"proxy_type"`
}

func (s *Service) JudgeServiceExistByName() error {
	return db.DB.Model(Service{}).Where("name = ?", s.Name).First(&s).Error
}

func (s *Service) JudgeServiceExistByNameNotSelf() error {
	return db.DB.Model(Service{}).Where("name = ?", s.Name).Not("id = ?", s.ID).First(&s).Error
}

func (s *Service) GetServiceByID(id uint64) error {
	return db.DB.Model(Service{}).Where("id = ?", id).First(&s).Error
}

func FindAllService(services *[]Service) error {
	return db.DB.Model(Service{}).Find(&services).Error
}

func (s *Service) DeleteService() error {
	return db.DB.Transaction(func(tx *gorm.DB) error {
		if err := tx.Exec("DELETE FROM services WHERE id = ?", s.ID).
			Exec("DELETE FROM http_rules WHERE service_id = ?", s.ID).
			Exec("DELETE FROM load_balances WHERE service_id = ?", s.ID).
			Exec("DELETE FROM service_controls WHERE service_id = ?", s.ID).Error; err != nil {
			return err
		}
		return nil
	})
}

func FindServices(search response.PageSearch, services *[]Service) error {
	result := db.DB.Model(Service{}).Limit(search.Size).Offset((search.Page - 1) * search.Size).Order("id desc")
	if search.Keyword != "" {
		return result.Where("name like ? or description like ?", "%"+search.Keyword+"%", "%"+search.Keyword+"%").Find(&services).Error
	}
	return result.Find(&services).Error
}

func GetServiceNum(num *int64) error {
	var services []Service
	return db.DB.Model(Service{}).Find(&services).Count(num).Error
}

func GroupByProxyType() ([]GroupByProxyTypeItem, error) {
	var res []GroupByProxyTypeItem
	if err := db.DB.Model(Service{}).Select("proxy_type, count(*) as value").Group("proxy_type").Scan(&res).Error; err != nil {
		return []GroupByProxyTypeItem{}, err
	}
	return res, nil
}

type GroupByProxyTypeItem struct {
	Name      string `json:"name"`
	ProxyType int8   `json:"proxy_type"`
	Value     int64  `json:"value"`
}
