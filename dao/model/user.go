package model

import (
	"gateway/setup/db"
	"time"
)

type User struct {
	ID           uint64        `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	CreatedAt    time.Time     `json:"created_at" gorm:"column:created_at"`
	UpdatedAt    time.Time     `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt    *time.Time    `json:"deleted_at" gorm:"column:deleted_at" sql:"index"`
	Name         string        `json:"name"`
	Password     string        `json:"password"`
	Avatar       string        `json:"avatar"`
	Introduction string        `json:"introduction"`
	BindAppCode  []UserAppCode `json:"bind_app_code" gorm:"foreignKey:UserID"`
}

type UserAppCode struct {
	ID      uint64 `json:"id" gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`
	UserID  uint64 `json:"user_id"`
	AppCode string `json:"app_code"`
}

func (a *User) CreateUser() error {
	return db.DB.Model(User{}).Create(a).Error
}

func (a *User) LoginUser() error {
	return db.DB.Model(User{}).Where("name = ? and password = ?", a.Name, a.Password).First(a).Error
}

func (a *User) GetUser() error {
	return db.DB.Model(User{}).Where("id = ?", a.ID).Preload("BindAppCode").First(a).Error
}
