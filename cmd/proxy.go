package cmd

import (
	"gateway/dao/manager"
	"gateway/reverse_proxy/discover"
	"gateway/router"
	"gateway/setup/config"
	"gateway/setup/db"
	"gateway/setup/etcd"
	"gateway/setup/log"
	"gateway/setup/redis"
	"gateway/util/common"

	"github.com/spf13/cobra"
)

var proxyCmd = &cobra.Command{
	Use:   "proxy",
	Short: "run gateway proxy server",
	Run: func(cmd *cobra.Command, args []string) {
		log.Init("gateway")
		cfg := config.InitConf()
		db.Init(cfg.MysqlConf)
		etcd.Init(cfg.EtcdConf)
		redis.Init(cfg.RedisConf)
		manager.ServiceManagerHandler.LoadOnce()
		manager.LoadBalancerHandler.LoadOnce()
		manager.AppManagerHandler.LoadOnce()
		discover.ServerManager.LoadServer(cfg.EtcdConf.Path)
		router := router.InitProxyRouter()
		if err := common.InitHttpServer(router, cfg.GatewayConf.Proxy).ListenAndServe(); err != nil {
			panic("server run err")
		}
	},
}

func init() {
	rootCmd.AddCommand(proxyCmd)
}
