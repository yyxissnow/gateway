package cmd

import (
	"gateway/router"
	"gateway/setup/config"
	"gateway/setup/db"
	"gateway/setup/log"
	"gateway/setup/redis"
	"gateway/util/common"

	"github.com/spf13/cobra"
)

var dashboardCmd = &cobra.Command{
	Use:   "dashboard",
	Short: "run gateway dashboard server",
	Run: func(cmd *cobra.Command, args []string) {
		log.Init("gateway")
		cfg := config.InitConf()
		db.Init(cfg.MysqlConf)
		redis.Init(cfg.RedisConf)
		router := router.InitDashboardRouter()
		if err := common.InitHttpServer(router, cfg.GatewayConf.Dashboard).ListenAndServe(); err != nil {
			panic("server run err")
		}
	},
}

func init() {
	rootCmd.AddCommand(dashboardCmd)
}
