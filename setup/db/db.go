package db

import (
	"fmt"
	"gateway/setup/config"
	"gateway/setup/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func Init(conf config.MysqlConf) {
	dsn := fmt.Sprintf(conf.Dsn, conf.Database)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup db err : %v", err))
	}
	sql, err := db.DB()
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup db sql err : %v", err))
	}
	sql.SetMaxIdleConns(conf.MaxIdleConn)
	sql.SetMaxOpenConns(conf.MaxOpenConn)
	DB = db
}
