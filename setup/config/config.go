package config

import (
	"fmt"
	"gateway/setup/log"
	"gopkg.in/ini.v1"
)

var cfg *WebConf

func InitConf() *WebConf {
	cfg = new(WebConf)
	err := ini.MapTo(cfg, "./config.ini")
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup config err : %v", err))
	}
	return cfg
}

func GetCfg() *WebConf {
	return cfg
}

type WebConf struct {
	GatewayConf `ini:"gateway"`
	MysqlConf   `ini:"mysql"`
	EtcdConf    `ini:"etcd"`
	RedisConf   `ini:"redis"`
}

type GatewayConf struct {
	Dashboard string `ini:"dashboard"`
	Proxy     string `ini:"proxy"`
}

type MysqlConf struct {
	Dsn         string `ini:"dsn"`
	Database    string `ini:"database"`
	MaxIdleConn int    `ini:"max_idle_conn"`
	MaxOpenConn int    `ini:"max_open_conn"`
}

type EtcdConf struct {
	Address string `ini:"address"`
	Timeout int64  `ini:"timeout"`
	Path    string `ini:"path"`
}

type RedisConf struct {
	Address  string `ini:"address"`
	Password string `ini:"password"`
}
