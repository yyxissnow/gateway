package etcd

import (
	"fmt"
	"gateway/setup/config"
	clientv3 "go.etcd.io/etcd/client/v3"
	"log"
	"time"
)

var client *clientv3.Client

func Init(conf config.EtcdConf) {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{conf.Address},
		DialTimeout: time.Duration(conf.Timeout) * time.Second,
	})
	if err != nil {
		log.Panic(fmt.Sprintf("etcd init err:%v", err))
		return
	}
	client = cli
}

func GetEtcd() *clientv3.Client {
	return client
}
