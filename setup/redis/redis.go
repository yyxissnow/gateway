package redis

import (
	"gateway/setup/config"
	"github.com/go-redis/redis"
	"log"
)

var client *redis.Client

func Init(conf config.RedisConf) {
	client = redis.NewClient(&redis.Options{
		Addr:     conf.Address,
		Password: conf.Password,
	})
	if pong, _ := client.Ping().Result(); pong != "PONG" {
		log.Panic("redis init err")
	}
}

func GetRedisClient() *redis.Client {
	return client
}
