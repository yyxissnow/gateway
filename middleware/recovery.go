package middleware

import (
	"fmt"
	"gateway/setup/log"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func RecoveryMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				log.Logger.Panic("happen panic!")
				response.ResError(c, response.ErrMiddlewarePanic, fmt.Sprintf("middleware panic :%v", err))
				c.Abort()
				return
			}
		}()
		c.Next()
	}
}
