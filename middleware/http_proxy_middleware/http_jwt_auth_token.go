package http_proxy_middleware

import (
	"fmt"
	"gateway/dao/manager"
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"strings"
)

func HttpJwtAuthTokenMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)
		//Authorization:Bearer xxx
		token := strings.ReplaceAll(c.GetHeader("Authorization"), "Bearer ", "")
		appMatched := false
		if token != "" {
			claims, err := common.JwtDecode(token)
			if err != nil {
				response.ResError(c, response.ErrDeCodeToken, fmt.Sprintf("decode token err:%v", err))
				c.Abort()
				return
			}
			appList := manager.AppManagerHandler.GetAppList()
			for _, app := range appList {
				if app.AppCode == claims.Issuer {
					c.Set("app", app)
					appMatched = true
					break
				}
			}
		}
		// 开启权限且没有匹配
		if serviceDetail.ServiceControl.OpenAuth == true && !appMatched {
			response.ResError(c, response.ErrNoMatchApp, "no match app")
			c.Abort()
			return
		}

		c.Next()
	}
}
