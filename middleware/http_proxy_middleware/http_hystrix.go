package http_proxy_middleware

import (
	"github.com/afex/hystrix-go/hystrix"
	"github.com/gin-gonic/gin"
)

func HttpHystrixMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		hystrix.ConfigureCommand("xxx", hystrix.CommandConfig{
			Timeout:                1000,
			MaxConcurrentRequests:  10,
			RequestVolumeThreshold: 1, //请求数量，10s采样，超过请求数才会采样
			SleepWindow:            5000,
			ErrorPercentThreshold:  50, //熔断验证，错误百分比
		})
	}
}
