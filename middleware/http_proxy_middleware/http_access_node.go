package http_proxy_middleware

import (
	"fmt"
	"gateway/dao/manager"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpAccessNodeMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		service, err := manager.ServiceManagerHandler.HttpAccessNode(c)
		if err != nil {
			response.ResError(c, response.ErrNoFindService, fmt.Sprintf("no find service:%v", err))
			c.Abort()
			return
		}
		c.Set("service", service)

		appList := manager.AppManagerHandler.GetAppList()
		for _, app := range appList {
			if app.AppCode == service.ServiceInfo.AppCode {
				c.Set("app", app)
				break
			}
		}

		c.Next()
	}
}
