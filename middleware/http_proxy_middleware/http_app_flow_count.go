package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpAppFlowCountMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		app, ok := c.Get("app")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxApp, "no get ctx app")
			c.Abort()
			return
		}
		appInfo := app.(*model.App)
		appCounter := common.FlowCounterHandler.GetCounter(common.FLowAppPrefix + appInfo.AppCode)
		appCounter.Increase()
		if appInfo.QPD > 0 && appCounter.TotalCount > appInfo.QPD {
			response.ResError(c, response.ErrMaxAppQPDFlow, "max app qpd flow")
			c.Abort()
			return
		}
		c.Next()
	}
}
