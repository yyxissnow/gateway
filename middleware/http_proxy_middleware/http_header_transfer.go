package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"strings"
)

func HttpHeaderTransferMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)
		for _, item := range strings.Split(serviceDetail.HttpRule.HeaderTransfer, ",") {
			change := strings.Split(item, " ")
			if len(change) != 3 {
				continue
			}
			if change[0] == "add" || change[0] == "edit" {
				c.Request.Header.Set(change[1], change[2])
			}
			if change[0] == "del" {
				c.Request.Header.Del(change[1])
			}
		}
		c.Next()
	}
}
