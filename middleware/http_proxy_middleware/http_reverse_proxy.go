package http_proxy_middleware

import (
	"fmt"
	"gateway/dao/manager"
	"gateway/dao/model"
	"gateway/reverse_proxy"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpReverseProxyMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		service, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		detail := service.(*model.ServiceDetail)

		//设置负载均衡器
		lb, err := manager.LoadBalancerHandler.GetLoadBalancer(detail)
		if err != nil {
			response.ResError(c, response.ErrNoFindServiceLoadBalancer, fmt.Sprintf("no find service load balancer :%v", err))
			c.Abort()
			return
		}
		trans := manager.TransportHandler.GetTransport(detail)
		proxy := reverse_proxy.NewLoadBalanceReverseProxy(c, lb, trans)
		proxy.ServeHTTP(c.Writer, c.Request) // 将原req传入
		c.Abort()
		return
	}
}
