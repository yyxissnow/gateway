package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"regexp"
	"strings"
)

func HttpUrlRewriteMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)
		// TODO：应该是只能转换一个地址
		for _, item := range strings.Split(serviceDetail.HttpRule.UrlRewrite, ",") {
			change := strings.Split(item, " ")
			if len(change) != 2 {
				continue
			}
			reg, err := regexp.Compile(change[0])
			if err != nil {
				continue
			}
			replacePath := reg.ReplaceAll([]byte(c.Request.URL.Path), []byte(change[1]))
			c.Request.URL.Path = string(replacePath)
		}
		c.Next()
	}
}
