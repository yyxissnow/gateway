package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpAppFlowLimitMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		app, ok := c.Get("app")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxApp, "no get ctx app")
			c.Abort()
			return
		}
		appInfo := app.(*model.App)
		if appInfo.QPS > 0 {
			appLimiter := common.FlowLimiterHandler.GetCounter(
				common.FLowAppPrefix+appInfo.AppCode+"_"+c.ClientIP(),
				float64(appInfo.QPS),
			)
			if !appLimiter.Allow() {
				response.ResError(c, response.ErrMaxAppLimitFlow, "max app limit flow")
				c.Abort()
				return
			}
		}
		c.Next()
	}
}
