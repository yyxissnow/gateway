package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"strings"
)

func HttpWhiteListMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)
		var list []string
		if serviceDetail.ServiceControl.WhiteList != "" {
			list = strings.Split(serviceDetail.ServiceControl.WhiteList, ",")
		}
		//TODO：是否要考虑 serviceDetail.ServiceControl.OpenAuth == true
		if len(list) > 0 {
			if !common.InStringSlice(list, c.ClientIP()) {
				response.ResError(c, response.ErrNoInServiceWhiteList, "no in service white list")
				c.Abort()
				return
			}
		}
		c.Next()
	}
}
