package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpServiceFlowLimitMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)

		if serviceDetail.ServiceControl.ServiceFlowLimit > 0 {
			qps := float64(serviceDetail.ServiceControl.ServiceFlowLimit)
			serviceLimiter := common.FlowLimiterHandler.GetCounter(
				common.FlowServicePrefix+serviceDetail.ServiceInfo.Name, qps)
			if !serviceLimiter.Allow() { // 桶中还有令牌就放行
				//TODO：是否要引用错误码
				response.ResError400(c, response.ErrMaxServiceLimitFlow, "max service limit flow")
				c.Abort()
				return
			}
		}

		if serviceDetail.ServiceControl.ClientIPFlowLimit > 0 {
			qps := float64(serviceDetail.ServiceControl.ClientIPFlowLimit)
			clientLimiter := common.FlowLimiterHandler.GetCounter(
				common.FlowServicePrefix+serviceDetail.ServiceInfo.Name+"_"+c.ClientIP(), qps)
			if !clientLimiter.Allow() { // 桶中还有令牌就放行
				response.ResError(c, response.ErrMaxClientLimitFlow, "max client limit flow")
				c.Abort()
				return
			}
		}

		c.Next()
	}
}
