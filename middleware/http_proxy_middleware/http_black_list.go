package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"strings"
)

func HttpBlackListMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)
		var whiteList []string
		if serviceDetail.ServiceControl.WhiteList != "" {
			whiteList = strings.Split(serviceDetail.ServiceControl.WhiteList, ",")
		}
		var blackList []string
		if serviceDetail.ServiceControl.BlackList != "" {
			blackList = strings.Split(serviceDetail.ServiceControl.BlackList, ",")
		}
		//白名单优先级大于黑名单,有白名单时黑名单失效(防止产生冲突)
		//TODO：是否要考虑 serviceDetail.ServiceControl.OpenAuth == true
		if len(whiteList) == 0 && len(blackList) > 0 {
			if common.InStringSlice(blackList, c.ClientIP()) {
				response.ResError(c, response.ErrInServiceBlackList, "in service black list")
				c.Abort()
				return
			}
		}
		c.Next()
	}
}
