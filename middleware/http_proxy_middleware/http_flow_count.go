package http_proxy_middleware

import (
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
)

func HttpServiceFlowCountMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		server, ok := c.Get("service")
		if !ok {
			response.ResError(c, response.ErrNoGetCtxService, "no get ctx service")
			c.Abort()
			return
		}
		serviceDetail := server.(*model.ServiceDetail)

		// 统计项 1 全站 2 服务 3 租户
		totalCounter := common.FlowCounterHandler.GetCounter(common.FlowTotal)
		totalCounter.Increase()

		serviceCounter := common.FlowCounterHandler.GetCounter(common.FlowServicePrefix + serviceDetail.ServiceInfo.Name)
		serviceCounter.Increase()

		c.Next()
	}
}
