package control

import (
	"fmt"
	"gateway/dao/dto"
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"time"
)

func GetPanelGroupData(c *gin.Context) {
	var serviceNum, appNum int64
	if err := model.GetServiceNum(&serviceNum); err != nil {
		response.ResError(c, response.ErrDBGetServiceNum, fmt.Sprintf("db get service num err:%v", err))
		return
	}
	if err := model.GetAppNum(&appNum); err != nil {
		response.ResError(c, response.ErrDBGetAppNum, fmt.Sprintf("db get app num err:%v", err))
		return
	}
	counter := common.FlowCounterHandler.GetCounter(common.FlowTotal)
	ack := dto.GetPanelGroupDataACK{
		ServiceNum:      serviceNum,
		AppNum:          appNum,
		CurrentQPS:      counter.QPS,
		TodayRequestNum: counter.TotalCount,
	}
	response.ResSuccessData(c, ack)
}

func StatisticsFlow(c *gin.Context) {
	counter := common.FlowCounterHandler.GetCounter(common.FlowTotal)
	currentTime := time.Now()
	var today []int64
	for i := 0; i <= currentTime.Hour(); i++ {
		dateTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		today = append(today, hourData)
	}
	var yesterday []int64
	yesterdayTime := currentTime.Add(-1 * time.Duration(time.Hour*24))
	for i := 0; i <= 23; i++ {
		dateTime := time.Date(yesterdayTime.Year(), yesterdayTime.Month(), yesterdayTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		yesterday = append(yesterday, hourData)
	}
	ack := dto.StatisticsFlowACK{
		Today:     today,
		Yesterday: yesterday,
	}
	response.ResSuccessData(c, ack)
}

func StatisticsService(c *gin.Context) {
	list, err := model.GroupByProxyType()
	if err != nil {
		response.ResError(c, response.ErrDBGroupByProxyType, fmt.Sprintf("db group by proxy type err:%s", err.Error()))
		return
	}
	var legend []string
	for i, item := range list {
		name, ok := common.ProxyTypeMap[item.ProxyType]
		if !ok {
			response.ResError(c, response.ErrProxyTypeMap, fmt.Sprintf("%v not proxy type", item.ProxyType))
			return
		}
		list[i].Name = name
		legend = append(legend, name)
	}
	ack := dto.StatisticsServiceACK{
		Legend: legend,
		Data:   list,
	}
	response.ResSuccessData(c, ack)
}
