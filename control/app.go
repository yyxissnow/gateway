package control

import (
	"errors"
	"fmt"
	"gateway/dao/dto"
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"strconv"
	"time"
)

func CreateApp(c *gin.Context) {
	var req dto.AddAppREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	app := model.App{
		AppCode:     req.AppCode,
		Name:        req.Name,
		Description: req.Description,
		Secret:      req.Secret,
		WhiteIPs:    req.WhiteIPS,
		QPD:         req.QPD,
		QPS:         req.QPS,
	}
	if err := app.GetAppByAppID(req.AppCode); !errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrDBAppIDExist, "app id already exist")
		return
	}
	if err := app.CreateApp(); err != nil {
		response.ResError(c, response.ErrDBCreateApp, fmt.Sprintf("db create app err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func UpdateApp(c *gin.Context) {
	var req dto.UpdateAppREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	app := model.App{
		ID:          req.ID,
		AppCode:     req.AppCode,
		Name:        req.Name,
		Description: req.Description,
		Secret:      req.Secret,
		WhiteIPs:    req.WhiteIPS,
		QPD:         req.QPD,
		QPS:         req.QPS,
	}
	if err := app.UpdateApp(); err != nil {
		response.ResError(c, response.ErrDBUpdateApp, fmt.Sprintf("db update app err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func DeleteApp(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var app model.App
	if err := app.GetAppByID(uint64(id)); errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrDBNoFindApp, "app no find")
		return
	}
	if err := app.DeleteApp(); err != nil {
		response.ResError(c, response.ErrDBDeleteApp, fmt.Sprintf("db delete app err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func GetApp(c *gin.Context) {
	var req dto.GetAppREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	var app model.App
	if err := app.GetAppByAppID(req.AppCode); err != nil {
		response.ResError(c, response.ErrDBGetApp, fmt.Sprintf("db get app err:%v", err))
		return
	}
	response.ResSuccessData(c, app)
}

func FindApps(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	size, _ := strconv.Atoi(c.DefaultQuery("size", "20"))
	search := response.PageSearch{
		Keyword: c.Query("keyword"),
		Page:    page,
		Size:    size,
	}
	var apps []model.App
	if err := model.FindApps(search, &apps); err != nil {
		response.ResError(c, response.ErrDBFindApps, fmt.Sprintf("db find apps err:%v", err))
		return
	}
	var ack []dto.FindAppsACK
	for _, app := range apps {
		counter := common.FlowCounterHandler.GetCounter(common.FLowAppPrefix + app.AppCode)
		ack = append(ack, dto.FindAppsACK{
			ID:          app.ID,
			AppCode:     app.AppCode,
			Name:        app.Name,
			Description: app.Description,
			Secret:      app.Secret,
			WhiteIPS:    app.WhiteIPs,
			QPS:         app.QPS,
			QPD:         app.QPD,
			RealQPD:     counter.TotalCount,
			RealQPS:     counter.QPS,
		})
	}
	response.ResSuccessData(c, response.ListResponse{
		Total: len(apps),
		List:  ack,
	})
}

func GetAppDetail(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var detail model.App
	if err := detail.GetAppByID(uint64(id)); err != nil {
		response.ResError(c, response.ErrDBGetAppDetail, fmt.Sprintf("get app detail err:%v", err))
		return
	}
	//TODO:dto
	response.ResSuccessData(c, detail)
}

func StatisticsApp(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var app model.App
	if err := app.GetAppByID(uint64(id)); errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrDBNoFindApp, "app no find")
		return
	}
	counter := common.FlowCounterHandler.GetCounter(common.FLowAppPrefix + app.AppCode)
	currentTime := time.Now()
	var today []int64
	for i := 0; i < currentTime.Hour(); i++ {
		dateTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		today = append(today, hourData)
	}
	var yesterday []int64
	yesterdayTime := currentTime.Add(-1 * (time.Hour * 24))
	for i := 0; i <= 23; i++ {
		dateTime := time.Date(yesterdayTime.Year(), yesterdayTime.Month(), yesterdayTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		yesterday = append(yesterday, hourData)
	}
	ack := dto.StatisticsAppACK{
		Today:     today,
		Yesterday: yesterday,
	}
	response.ResSuccessData(c, ack)
}
