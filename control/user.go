package control

import (
	"fmt"
	"gateway/dao/dto"
	"gateway/dao/model"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"strconv"
)

func CreateUser(c *gin.Context) {
	var req dto.CreateUserREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	admin := model.User{
		Name:         req.Name,
		Password:     req.Password,
		Avatar:       req.Avatar,
		Introduction: req.Introduction,
	}
	if err := admin.CreateUser(); err != nil {
		response.ResError(c, response.ErrDBCreateAdmin, fmt.Sprintf("db create admin err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func LoginUser(c *gin.Context) {
	var req dto.LoginUserREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	admin := model.User{
		Name:     req.Name,
		Password: req.Password,
	}
	if err := admin.LoginUser(); err != nil {
		response.ResError(c, response.ErrDBLoginAdmin, fmt.Sprintf("admin login err:%v", err))
		return
	}
	ack := dto.LoginUserACK{
		Token: strconv.Itoa(int(admin.ID)),
	}
	response.ResSuccessData(c, ack)
}

func LogoutUser(c *gin.Context) {
	response.ResSuccess(c)
}

func GetInfo(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("token"))
	admin := model.User{
		ID: uint64(id),
	}
	if err := admin.GetUser(); err != nil {
		response.ResError(c, response.ErrDBGetAdmin, fmt.Sprintf("get admin err:%v", err))
		return
	}
	ack := dto.GetUserACK{
		ID:           admin.ID,
		Name:         admin.Name,
		Avatar:       admin.Avatar,
		Introduction: admin.Introduction,
		Roles:        []string{"admin"},
	}
	response.ResSuccessData(c, ack)
}
