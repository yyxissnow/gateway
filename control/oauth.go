package control

import (
	"encoding/base64"
	"fmt"
	"gateway/dao/dto"
	"gateway/dao/manager"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"strings"
	"time"
)

func GetToken(c *gin.Context) {
	// Authorization:Basic xxx
	splits := strings.Split(c.GetHeader("Authorization"), " ")
	if len(splits) != 2 {
		response.ResError(c, response.ErrRequestParam, "authorization len err")
		return
	}
	secret, err := base64.StdEncoding.DecodeString(splits[1])
	if err != nil {
		response.ResError(c, response.ErrDecodeString, fmt.Sprintf("decode authorization err:%v", err))
		return
	}
	// user_name:password
	parts := strings.Split(string(secret), ":")
	if len(parts) != 2 {
		response.ResError(c, response.ErrRequestParam, "user password len err")
		return
	}
	if !manager.AppManagerHandler.ExistApp(parts[0], parts[1]) {
		response.ResError(c, response.ErrNoFindApp, "manager no find app")
		return
	}
	claims := jwt.StandardClaims{
		Issuer:    parts[0],
		ExpiresAt: time.Now().Add(common.JwtExpires * time.Second).In(time.Local).Unix(),
	}
	token, err := common.JwtEncode(claims)
	if err != nil {
		response.ResError(c, response.ErrEnCodeToken, fmt.Sprintf("decode jwt token err:%v", err))
		return
	}
	ack := dto.GetTokenRSP{
		Token:     token,
		Expires:   common.JwtExpires,
		TokenType: "Bearer",
		Scope:     "read_write",
	}
	response.ResSuccessData(c, ack)
}
