package control

import (
	"errors"
	"fmt"
	"gateway/dao/dto"
	"gateway/dao/model"
	"gateway/util/common"
	"gateway/util/response"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"strconv"
	"strings"
	"time"
)

func FindServices(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	size, _ := strconv.Atoi(c.DefaultQuery("size", "20"))
	search := response.PageSearch{
		Keyword: c.Query("keyword"),
		Page:    page,
		Size:    size,
	}
	var services []model.Service
	if err := model.FindServices(search, &services); err != nil {
		response.ResError(c, response.ErrDBFindServices, fmt.Sprintf("db find services err:%v", err))
		return
	}
	var ack []dto.FindServicesACK
	for _, service := range services {
		var detail model.ServiceDetail
		if err := detail.GetServiceDetail(service.ID); err != nil {
			response.ResError(c, response.ErrDBGetServiceDetail, fmt.Sprintf("get service detail err:%v", err))
			return
		}

		counter := common.FlowCounterHandler.GetCounter(common.FlowServicePrefix + service.Name)
		ack = append(ack, dto.FindServicesACK{
			ID:          service.ID,
			Name:        service.Name,
			Description: service.Description,
			ProxyType:   service.ProxyType,
			Address:     detail.GetServiceAddress(),
			QPS:         counter.QPS,
			QPD:         counter.TotalCount,
			Nodes:       len(detail.LoadBalance.GetIPNodes()),
		})
	}
	response.ResSuccessData(c, response.ListResponse{
		Total: len(services),
		List:  ack,
	})
}

func GetServiceDetail(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var detail model.ServiceDetail
	if err := detail.GetServiceDetail(uint64(id)); err != nil {
		response.ResError(c, response.ErrDBGetServiceDetail, fmt.Sprintf("get service detail err:%v", err))
		return
	}
	//TODO:dto
	response.ResSuccessData(c, detail)
}

func ServiceStatistics(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var service model.Service
	if err := service.GetServiceByID(uint64(id)); errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrDBNoFindService, "no find service")
		return
	}

	counter := common.FlowCounterHandler.GetCounter(common.FlowServicePrefix + service.Name)
	currentTime := time.Now()
	var today []int64
	for i := 0; i <= currentTime.Hour(); i++ {
		dateTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		today = append(today, hourData)
	}
	var yesterday []int64
	yesterdayTime := currentTime.Add(-1 * (time.Hour * 24))
	for i := 0; i <= 23; i++ {
		dateTime := time.Date(yesterdayTime.Year(), yesterdayTime.Month(), yesterdayTime.Day(), i, 0, 0, 0, time.Local)
		hourData, _ := counter.GetHourData(dateTime)
		yesterday = append(yesterday, hourData)
	}
	response.ResSuccessData(c, dto.ServiceStatisticsACK{
		Today:     today,
		Yesterday: yesterday,
	})
}

func DeleteService(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	var service model.Service
	if err := service.GetServiceByID(uint64(id)); errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrNoFindService, "no find service")
		return
	}
	if err := service.DeleteService(); err != nil {
		response.ResError(c, response.ErrDBDeleteService, fmt.Sprintf("db delete service err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func CreateHttpService(c *gin.Context) {
	var req dto.CreateHttpServiceREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	if len(strings.Split(req.IPList, ",")) != len(strings.Split(req.WeightList, ",")) {
		response.ResError(c, response.ErrRequestParam, "ip list len not equal weight list len")
		return
	}
	if err := model.JudgeAppExist(req.AppCode); errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrDBNoFindApp, fmt.Sprintf("no find app:%v", err))
		return
	}
	service := model.Service{
		Name:        req.Name,
		Description: req.Description,
		AppCode:     req.AppCode,
		ProxyType:   model.Http,
	}
	rule := model.HttpRule{
		RuleType:       req.RuleType,
		Rule:           req.Rule,
		NeedHttps:      req.NeedHttps,
		NeedWebsocket:  req.NeedWebSocket,
		UrlRewrite:     req.UrlRewrite,
		HeaderTransfer: req.HeaderTransfer,
	}
	control := model.ServiceControl{
		OpenAuth:          req.OpenAuth,
		WhiteList:         req.WhiteList,
		BlackList:         req.BackList,
		ClientIPFlowLimit: req.ClientIPFlowLimit,
		ServiceFlowLimit:  req.ServiceFlowLimit,
	}
	load := model.LoadBalance{
		LoadBalanceType:        req.LoadBalanceType,
		IPList:                 req.IPList,
		WeightList:             req.WeightList,
		UpstreamConnectTimeout: req.UpstreamConnectTimeout,
		UpstreamHeaderTimeout:  req.UpstreamHeaderTimeout,
		UpstreamIdleTimeout:    req.UpstreamIdleTimeout,
		UpstreamMaxIdle:        req.UpstreamMaxIdle,
	}
	if err := rule.CreateHttpService(&service, &control, &load); err != nil {
		response.ResError(c, response.ErrDBCreateHttpService, fmt.Sprintf("db create http service err:%v", err))
		return
	}
	response.ResSuccess(c)
}

func UpdateHttpService(c *gin.Context) {
	var req dto.UpdateServiceHttpREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("bind json err:%v", err))
		return
	}
	if len(strings.Split(req.IPList, ",")) != len(strings.Split(req.WeightList, ",")) {
		response.ResError(c, response.ErrRequestParam, "ip list len not equal weight list len")
		return
	}
	service := model.Service{
		ID:          req.ServiceID,
		Name:        req.Name,
		Description: req.Description,
		AppCode:     req.AppCode,
		ProxyType:   model.Http,
	}
	rule := model.HttpRule{
		ServiceID:      req.ServiceID,
		RuleType:       req.RuleType,
		Rule:           req.Rule,
		NeedHttps:      req.NeedHttps,
		NeedWebsocket:  req.NeedWebSocket,
		UrlRewrite:     req.UrlRewrite,
		HeaderTransfer: req.HeaderTransfer,
	}
	control := model.ServiceControl{
		ServiceID:         req.ServiceID,
		OpenAuth:          req.OpenAuth,
		WhiteList:         req.WhiteList,
		BlackList:         req.BackList,
		ClientIPFlowLimit: req.ClientIPFlowLimit,
		ServiceFlowLimit:  req.ServiceFlowLimit,
	}
	load := model.LoadBalance{
		ServiceID:              req.ServiceID,
		LoadBalanceType:        req.LoadBalanceType,
		IPList:                 req.IPList,
		WeightList:             req.WeightList,
		UpstreamConnectTimeout: req.UpstreamConnectTimeout,
		UpstreamHeaderTimeout:  req.UpstreamHeaderTimeout,
		UpstreamIdleTimeout:    req.UpstreamIdleTimeout,
		UpstreamMaxIdle:        req.UpstreamMaxIdle,
	}
	if err := model.UpdateHttpService(&service, &rule, &control, &load); err != nil {
		response.ResError(c, response.ErrDBUpdateHttpService, fmt.Sprintf("db update http service err:%v", err))
		return
	}
	response.ResSuccess(c)
}
